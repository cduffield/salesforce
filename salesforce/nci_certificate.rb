module Salesforce

  class NciCertificate
    CLIENT = Restforce.new :username => ENV['SFDC_USERNAME'],
      :password => ENV['SFDC_PASSWORD'],
      :security_token => ENV['SFDC_SECURITY_TOKEN'],
      :client_id => ENV['SFDC_CLIENT_ID'],
      :client_secret => ENV['SFDC_CLIENT_SECRET'],
      :host => ENV['SFDC_HOST']
    private_constant :CLIENT

    attr_accessor :nci_certificate

    def initialize(nci_certificate_object)
      @nci_certificate = nci_certificate_object
    end

    def call
      certification = Certification.find_by(name: certification_type)
      salesforce_contact = SalesforceContact.find_by(salesforce_id: user_sf_id)
      unless certification.nil? || salesforce_contact.nil?
        if salesforce_contact.certification_users.exists?(certification_id: certification[:id])
          certification_user = CertificationUser.find_by(certification_id: certification[:id],
                                                    salesforce_contact_id: salesforce_contact[:id])
          certification_user.expires_at = expiration_date
          if certification_user.save
            Salesforce::NciCertificate.update_cmupdate(update_object_id, "Updated")
          else
            Salesforce::NciCertificate.update_cmupdate(update_object_id, "Error")
          end
        else
          salesforce_contact.certification_users.build(certification_id: certification[:id],
                                         expires_at: expiration_date)
          if salesforce_contact.save
            Salesforce::NciCertificate.update_cmupdate(update_object_id, "Updated")
          else
            Salesforce::NciCertificate.update_cmupdate(update_object_id, "Error")
          end
        end
      end
    end

    def update_object_id
      nci_certificate[:Id]
    end

    def certification_type
      nci_certificate[:Type__c]
    end

    def user_sf_id
      nci_certificate[:Contact_ID__c]
    end

    def certification_expiration
      nci_certificate.key?(:Expiration__c) && nci_certificate[:Expiration__c] ? nci_certificate[:Expiration__c].to_date : nil
    end

    def certification_extended_expiration
      nci_certificate.key?(:Extended_Expiration__c) && nci_certificate[:Extended_Expiration__c] ? nci_certificate[:Extended_Expiration__c].to_date : nil
    end

    def expiration_date
      expiration = certification_expiration.present? ? Date.strptime(certification_expiration.to_s, '%Y-%m-%d') : Date.new
      extended_expiration = certification_extended_expiration.present? ? Date.strptime(certification_extended_expiration.to_s, '%Y-%m-%d') : Date.new

      if extended_expiration > expiration
        certification_extended_expiration
      else
        certification_expiration
      end
    end

    def self.expiration(nci_cert)
      expiration = nci_cert.expr0.present? ? Date.strptime(nci_cert.expr0, '%Y-%m-%d') : Date.new
      extended_expiration = nci_cert.expr1.present? ? Date.strptime(nci_cert.expr1, '%Y-%m-%d') : Date.new

      if extended_expiration > expiration
        nci_cert.expr1
      else
        nci_cert.expr0
      end
    end

    def self.update_cmupdate(salesforce_id, status)
      client = CLIENT
      client.update('ComfortMaxx_Update__C', Id: salesforce_id, Status__c: status)
    end

    def self.get_nci_certificates(contact_ids)
      ids = contact_ids.map { |x| "'#{x}'" }.join(',')
      client = CLIENT
      nci_certificates = client.query("SELECT Type__c, Contact__c, MAX(Expiration__c), MAX(Extended_Expiration__c) FROM NCI_Certificates__c
        where Contact__c in (#{ids})
        Group By Type__c, Contact__c")
    end

    def self.get_all_nci_certificates

      client = CLIENT
      nci_certs = client.query("SELECT Type__c, MAX(Expiration__c), MAX(Extended_Expiration__c), Contact__c
          FROM NCI_Certificates__c
          WHERE Contact__r.CM_User_Id__c > 0
          AND Type__c IN ('Air Testing & Diagnostics', 'Air Balancing Commercial', 'Air Balancing Residential', 'Air Balancing Residential/Light Commercial', 'NBC Certification', 'NBC Technician', 'System Performance Commercial', 'System Performance Light Commercial', 'System Performance Residential')
          GROUP By Type__c, Contact__c ORDER BY Contact__c ASC")

      nci_certs.each do |nci_cert|
        nci_cert.deep_symbolize_keys
        certification = Certification.find_by(name: nci_cert[:Type__c])
        salesforce_contact = SalesforceContact.find_by(salesforce_id: nci_cert[:Contact__c])
        unless certification.nil? || salesforce_contact.nil?
          if salesforce_contact.certification_users.exists?(certification_id: certification[:id])
            certification_user = CertificationUser.find_by(certification_id: certification[:id],
                                                           salesforce_contact_id: salesforce_contact[:id])
            certification_user.expires_at = expiration(nci_cert)
            certification_user.save
          else
            salesforce_contact.certification_users.build(certification_id: certification[:id],
                                                         expires_at: expiration(nci_cert))
            salesforce_contact.save
          end
        end
      end
    end

  end
end
