module Salesforce

  class Contract
    CLIENT = Restforce.new :username => ENV['SFDC_USERNAME'],
      :password => ENV['SFDC_PASSWORD'],
      :security_token => ENV['SFDC_SECURITY_TOKEN'],
      :client_id => ENV['SFDC_CLIENT_ID'],
      :client_secret => ENV['SFDC_CLIENT_SECRET'],
      :host => ENV['SFDC_HOST']
    private_constant :CLIENT

    attr_accessor :contract

    def initialize(contract_object)
      @contract = contract_object
    end

    def call
      organization = Organization.find_by(salesforce_id: organization_sf_id)
      subscription = Subscription.find_by(name: contract_type)
      unless organization.nil? || subscription.nil?
        if status == 'Activated' || status == 'Draft'
          if organization.organization_subscriptions.exists?(subscription_id: subscription[:id])
            Salesforce::Contract.update_cmupdate(update_object_id, "Updated")
          else
            organization.organization_subscriptions.build(subscription_id: subscription[:id])
            if organization.save
              Salesforce::Contract.update_cmupdate(update_object_id, "Updated")
            else
              Salesforce::Contract.update_cmupdate(update_object_id, "Error")
            end
          end
        else
          if organization.organization_subscriptions.where(subscription_id: subscription[:id]).destroy_all
            Salesforce::Contract.update_cmupdate(update_object_id, "Updated")
          else
            Salesforce::Contract.update_cmupdate(update_object_id, "Error")
          end
        end
      end
    end

    def update_object_id
      contract[:Id]
    end

    def organization_sf_id
      contract[:Account_Id__c]
    end

    def status
      contract[:Contract_Status__c]
    end

    def contract_type
      contract[:Contract_Type__c]
    end

    def self.update_cmupdate(salesforce_id, status)
      client = CLIENT
      client.update('ComfortMaxx_Update__C', Id: salesforce_id, Status__c: status)
    end

    def self.get_contracts(account_id)
      client = CLIENT
      contracts = client.query("SELECT Id, AccountId, Status, Type__c from Contract where Status in ('Draft', 'Activated') and AccountId = '#{account_id}'")
    end

    def self.get_all_contracts
      client = CLIENT
      salesforce_ids = Organization.where("salesforce_id IS NOT NULL").pluck(:salesforce_id)
      account_ids = salesforce_ids.map { |x| "'#{x}'" }.join(',')

      unless account_ids.empty?
        contracts = client.query("SELECT Id, AccountId, Status, Type__c from Contract where Status in ('Draft', 'Activated') and AccountId in (#{account_ids})")
        contracts.each do |contract|
          contract.deep_symbolize_keys
          organization = Organization.find_by(salesforce_id: contract[:AccountId])
          subscription = Subscription.find_by(name: contract[:Type__c])
          unless organization.nil? || subscription.nil?
            if contract[:Status] == 'Activated' || contract[:Status] == 'Draft'
              unless organization.organization_subscriptions.exists?(subscription_id: subscription[:id])
                organization.organization_subscriptions.build(subscription_id: subscription[:id])
                organization.save
              end
            end
          end
        end
      end
    end

  end
end
