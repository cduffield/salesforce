module Salesforce

  class Account
    CLIENT = Restforce.new :username => ENV['SFDC_USERNAME'],
      :password => ENV['SFDC_PASSWORD'],
      :security_token => ENV['SFDC_SECURITY_TOKEN'],
      :client_id => ENV['SFDC_CLIENT_ID'],
      :client_secret => ENV['SFDC_CLIENT_SECRET'],
      :host => ENV['SFDC_HOST']
    private_constant :CLIENT

    attr_accessor :account
    attr_accessor :event

    def initialize(account_object)
      @account = account_object
    end

    def self.update_salesforce(salesforce_id, status, id)
      client = CLIENT
      client.update('Account', Id: salesforce_id,
                               CM_Org_Status__c: status,
                               CM_Org_ID__c: id)
    end

    def self.update_salesforce_setup(salesforce_id, organization)
      client = CLIENT
      client.update('Account', Id: salesforce_id,
                               CM_Org_Name__c: organization.name,
                               CM_Org_Contact_Name__c: organization.contact_name,
                               CM_Org_Phone__c: organization.contact_phone,
                               CM_Org_Street_Address__c: organization.street_address,
                               CM_Org_City__c: organization.city,
                               CM_Org_Country__c: organization.country,
                               CM_Org_Region__c: organization.region,
                               CM_Org_Postal_Code__c: organization.postal_code)
    end

    def self.update_cmupdate(salesforce_id, status)
      client = CLIENT
      client.update('ComfortMaxx_Update__C', Id: salesforce_id, Status__c: status)
    end

    def self.update_all_salesforce_accounts
      organizations = Organization.where.not(salesforce_id: nil)

      organizations.each do |organization|
        Salesforce::Account.update_salesforce(organization.salesforce_id, 'Active', organization.id)
        Salesforce::Account.update_salesforce_setup(organization.salesforce_id,  organization)
      end
    end

    def call
      unless deleted?
        if sf_id.present?
          organization = Organization.find_by(salesforce_id: sf_id)
          puts organization.inspect
          if organization.nil?
            create_organization
          else
            if organization_updated?(organization)
              update_organization(organization)
            end
          end
        end
      end
    end

    def organization_updated?(organization)
      if organization.contact_phone != sanitize(sf_contact_phone) ||
          organization.contact_name != sf_contact_name ||
          organization.street_address != sf_street_address ||
          organization.city != sf_city ||
          organization.region != sf_region ||
          organization.postal_code != sf_postal_code ||
          organization.country != sf_country ||
          organization.active != sf_active
          true
      else
        false
      end
    end

    def update_organization(organization)
     organization.name = sf_name
      organization.contact_name = sf_contact_name
      organization.contact_phone = sf_contact_phone
      organization.street_address = sf_street_address
      organization.city = sf_city
      organization.region = sf_region
      organization.postal_code = sf_postal_code
      organization.country = sf_country
      organization.active = sf_active
      if organization.save
        Salesforce::Account.update_cmupdate(update_object_id, "Updated")
      else
        Salesforce::Account.update_cmupdate(update_object_id, "Error")
      end
    end

    def create_organization
      organization = Organization.new(name: sf_name,
                                      contact_name: sf_contact_name,
                                      contact_phone: sf_contact_phone,
                                      street_address: sf_street_address,
                                      city: sf_city,
                                      region: sf_region,
                                      postal_code: sf_postal_code,
                                      country: sf_country,
                                      salesforce_id: sf_id,
                                      account_number: 'FIXME')
      if organization.save
        Salesforce::Account.update_salesforce(sf_id, 'Active', organization.id)
        Salesforce::Account.update_cmupdate(update_object_id, "Updated")
        create_org_info(organization)
      else
        Salesforce::Account.update_cmupdate(update_object_id, "Error")
      end
    end

    def create_org_info(organization)
      contacts = Salesforce::Contact.get_contacts(sf_id)
      contact_ids = []
      contacts.each do |contact|
        contact.deep_symbolize_keys
        sf_contact = SalesforceContact.create(salesforce_id: contact[:Id])
        user = User.new(first_name: contact[:FirstName],
                        last_name: contact[:LastName],
                        salesforce_contact: sf_contact,
                        password: SecureRandom.hex(20)
                       )
        user.password_confirmation = user.password
        user.build_membership(
          organization: organization,
          role: 0,
          active: true
        )
        if user.save_employee
          contact_ids << contact[:Id]
          Salesforce::Contact.update_user(user, 'Active') unless user.salesforce_contact.nil?
          Salesforce::Contact.update_salesforce(user, 'Active') unless user.salesforce_contact.nil?
        else
          # log error
        end
      end

      unless contact_ids.empty?
        nci_certs = Salesforce::NciCertificate.get_nci_certificates(contact_ids)
        nci_certs.each do |nci_cert|
          nci_cert.deep_symbolize_keys
          certification = Certification.find_by(name: nci_cert[:Type__c])
          salesforce_contact = SalesforceContact.find_by(salesforce_id: nci_cert[:Contact__c])
          unless certification.nil? || salesforce_contact.nil?
            if salesforce_contact.certification_users.exists?(certification_id: certification[:id])
              certification_user = CertificationUser.find_by(certification_id: certification[:id],
                                                        salesforce_contact_id: salesforce_contact[:id])
              certification_user.expires_at = expriration_date(nci_cert)
              certification_user.save
            else
              salesforce_contact.certification_users.build(certification_id: certification[:id],
                                             expires_at: expiration_date(nci_cert))
              salesforce_contact.save
            end
          end
        end
      end

      contracts = Salesforce::Contract.get_contracts(sf_id)
      contracts.each do |contract|
        contract.deep_symbolize_keys
        subscription = Subscription.find_by(name: contract[:Type__c])
        unless organization.nil? || subscription.nil?
          if contract[:Status] == 'Activated' || contract[:Status] == 'Draft'
            unless organization.organization_subscriptions.exists?(subscription_id: subscription[:id])
              organization.organization_subscriptions.build(subscription_id: subscription[:id])
              organization.save
            end
          end
        end
      end
    end

    def expiration_date(nci_cert)
      expiration = nci_cert.expr0.present? ? Date.strptime(nci_cert.expr0, '%Y-%m-%d') : Date.new
      extended_expiration = nci_cert.expr1.present? ? Date.strptime(nci_cert.expr1, '%Y-%m-%d') : Date.new

      if extended_expiration > expiration
        nci_cert.expr1
      else
        nci_cert.expr0
      end
    end

    def deleted?
      account[:Object_Event__c] == 'Deleted' ? true : false
    end

    def update_object_id
      account[:Id]
    end

    def sf_id
      account[:ID__c]
    end

    def sf_name
      account[:CM_Org_Name__c]
    end

    def sf_contact_name
      account[:CM_Org_Contact_Name__c]
    end

    def sf_contact_phone
      account[:CM_Org_Phone__c]
    end

    def sf_street_address
      account[:CM_Org_Street_Address__c]
    end

    def sf_city
      account[:CM_Org_City__c]
    end

    def sf_region
      account[:CM_Org_Region__c]
    end

    def sf_postal_code
      account[:CM_Org_Postal_Code__c]
    end

    def sf_country
      account[:CM_Org_Country__c]
    end

    def sf_active
      account[:CM_Org_Status__c] == 'Active' ? true : false
    end

    def sanitize(value)
      value.gsub(/\D+/,'')
    end

  end
end
