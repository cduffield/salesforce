module Salesforce

  class Contact
    CLIENT = Restforce.new :username => ENV['SFDC_USERNAME'],
      :password => ENV['SFDC_PASSWORD'],
      :security_token => ENV['SFDC_SECURITY_TOKEN'],
      :client_id => ENV['SFDC_CLIENT_ID'],
      :client_secret => ENV['SFDC_CLIENT_SECRET'],
      :host => ENV['SFDC_HOST']
    private_constant :CLIENT

    attr_accessor :contact
    attr_accessor :event

    def initialize(contact_object)
      @contact = contact_object
    end

    def call
      unless deleted?
        if invited?
          invite_employee
        elsif deactivated?
          deactivate_employee
        elsif activating?
          activate_employee
        elsif activated?
          org = Organization.find_by(salesforce_id: account_id)
          sf_contact = SalesforceContact.find_by(salesforce_id: sf_id)
          user = org.users.find_by(salesforce_contact: sf_contact)
          if user.nil?
            add_employee
          else
            if user_updated?(user)
              update_user(user)
            end
            if user.current_employee == false
              activate_user(user, sf_contact)
            end
          end
        end
      end
    end

    def user_updated?(user)
      if user.email != sf_email ||
          user.first_name != sf_first_name ||
          user.last_name != sf_last_name ||
          user.role != sf_role.capitalize
        true
      else
        false
      end
    end

    def update_user(user)
      user.email = sf_email
      user.first_name = sf_first_name
      user.last_name = sf_last_name
      user.membership.role = sf_role
      if user.save_employee
        Salesforce::Contact.update_cmupdate(update_object_id, "Updated")
        state = status(user)
        Salesforce::Contact.update_user(user, state)
      else
        Salesforce::Contact.update_cmupdate(update_object_id, "Error")
      end
    end

    def invite_user
      organization = Organization.find_by(salesforce_id: account_id)
      salesforce_contact = SalesforceContact.find_by(salesforce_id: sf_id)
      user = organization.users.find_by(salesforce_contact: salesforce_contact)
      if account_id.present?
        @invitation = Invitation.new(first_name: sf_first_name,
                                     last_name: sf_last_name,
                                     email: sf_email,
                                     role: sf_role,
                                     organization_id: organization.id)
        if @invitation.save
          inactive_user(salesforce_contact).save
          UserMailer.invite(@invitation).deliver_now
          Salesforce::Contact.update_salesforce(user, 'Invited')
          Salesforce::Contact.update_cmupdate(update_object_id, "Updated")
        else
          Salesforce::Contact.update_cmupdate(update_object_id, "Error")
        end
      end
    end

    def invite_employee
      organization = Organization.find_by(salesforce_id: account_id)
      salesforce_contact = SalesforceContact.find_by(salesforce_id: sf_id)
      user = organization.users.find_by(salesforce_contact: salesforce_contact)
      unless user.nil?
        @invitation = Invitation.new(first_name: sf_first_name,
                                     last_name: sf_last_name,
                                     email: sf_email,
                                     role: sf_role,
                                     organization_id: organization.id)
        if @invitation.save
          user.email = sf_email
          user.membership.role =  sf_role
          user.password = SecureRandom.hex(20)
          user.password_confirmation = user.password
          if user.save
            UserMailer.invite(@invitation).deliver_now
            Salesforce::Contact.update_salesforce(user, 'Invited')
            Salesforce::Contact.update_cmupdate(update_object_id, "Updated")
          else
            Salesforce::Contact.update_cmupdate(update_object_id, "Error")
          end
        else
          Salesforce::Contact.update_cmupdate(update_object_id, "Error")
        end
      end
    end

    def add_employee
        organization = Organization.find_by(salesforce_id: account_id)
        sf_contact = SalesforceContact.find_or_create_by(salesforce_id: sf_id)
        memberships = Membership.joins(:user).where(users: { salesforce_contact_id: sf_contact.id })
        memberships.update_all(role: 0)
        memberships.update_all(active: false)
        sf_contact.users.update_all(email: nil)
        sf_contact.users.update_all(current_employee: false)
        user = User.new(first_name: sf_first_name,
                        last_name: sf_last_name,
                        salesforce_contact: sf_contact,
                        password: SecureRandom.hex(20)
                       )
        user.password_confirmation = user.password
        user.build_membership(
          organization: organization,
          role: sf_role,
          active: true
        )
        if user.save_employee
          Salesforce::Contact.update_salesforce(user, 'Active')
          Salesforce::Contact.update_cmupdate(update_object_id, "Updated")
        else
          Salesforce::Contact.update_cmupdate(update_object_id, "Error")
        end
    end

    def activate_user(user, sf_contact)
      memberships = Membership.joins(:user).where(users: { salesforce_contact_id: user.salesforce_contact.id })
      members = memberships.where.not(user_id: user.id)
      members.update_all(role: 0)
      members.update_all(active: false)
      users = sf_contact.users.where.not(id: user.id)
      users.update_all(email: nil)
      users.update_all(current_employee: false)
      user.current_employee = true
      user.membership.active = true
      if user.save_employee
        Salesforce::Contact.update_salesforce(user, 'Active')
        Salesforce::Contact.update_cmupdate(update_object_id, "Updated")
      else
        Salesforce::Contact.update_cmupdate(update_object_id, "Error")
      end
    end

    def deactivate_employee
      organization = Organization.find_by(salesforce_id: account_id)
      salesforce_contact = SalesforceContact.find_by(salesforce_id: sf_id)
      user = organization.users.find_by(salesforce_contact: salesforce_contact)
      unless user.nil?
        user.membership.active = false
        if user.save_employee
          Salesforce::Contact.update_user(user, 'Deactivated')
          Salesforce::Contact.update_cmupdate(update_object_id, "Updated")
        else
          Salesforce::Contact.update_cmupdate(update_object_id, "Error")
        end
      end
    end

    def activate_employee
      organization = Organization.find_by(salesforce_id: account_id)
      salesforce_contact = SalesforceContact.find_by(salesforce_id: sf_id)
      user = organization.users.find_by(salesforce_contact: salesforce_contact)
      memberships = Membership.joins(:user).where(users: { salesforce_contact_id: user.salesforce_contact.id })
      members = memberships.where.not(user_id: user.id)
      members.update_all(role: 0)
      members.update_all(active: false)
      users = salesforce_contact.users.where.not(id: user.id)
      users.update_all(email: nil)
      users.update_all(current_employee: false)
      user.current_employee = true
      user.membership.active = true
      unless user.nil?
        user.membership.active = true
        if user.save_employee
          Salesforce::Contact.update_user(user, 'Active')
          Salesforce::Contact.update_cmupdate(update_object_id, "Updated")
        else
          Salesforce::Contact.update_cmupdate(update_object_id, "Error")
        end
      end
    end

    def created?
      event[:type] == "created" ? true : false
    end

    def updated?
      event[:type] == "updated" ? true : false
    end

    def update_object_id
      contact[:Id]
    end

    def sf_id
      contact[:ID__c]
    end

    def sf_first_name
      contact[:CM_User_First_Name__c]
    end

    def sf_last_name
      contact[:CM_User_Last_Name__c]
    end

    def sf_email
      contact[:CM_User_Email__c]
    end

    def sf_role
      contact[:CM_User_Role__c].parameterize.underscore
    end

    def account_id
      contact[:CM_User_Account_ID__c]
    end

    def deleted?
      contact[:Object_Event__c] == 'Deleted' ? true : false
    end

    def invited?
      contact[:Object_Event__c] == 'Invite' ? true : false
    end

    def deactivated?
      contact[:CM_User_Status__c] == 'Deactivating' ? true : false
    end

    def activated?
      contact[:CM_User_Status__c] == 'Pending' ? true : false
    end

    def activating?
      contact[:CM_User_Status__c] == 'Activating' ? true : false
    end

    def status(user)
      user.membership.active? ? "Active" : "Deactivated"
    end

    def self.get_contacts(account_id)
      client = CLIENT
      contacts = client.query("SELECT Id, FirstName, LastName, AccountId FROM Contact where AccountId = '#{account_id}'")
    end

    def self.update_salesforce(user, status)
      salesforce_id = user.salesforce_contact.salesforce_id
      client = CLIENT
      client.update('Contact', Id: salesforce_id,
                               CM_User_Status__c: status,
                               CM_User_ID__c: user.id)
    end

    def self.update_user(user, status)
      salesforce_id = user.salesforce_contact.salesforce_id
      client = CLIENT
      client.update('Contact', Id: salesforce_id,
                               CM_User_First_Name__c: user.first_name,
                               CM_User_Last_Name__c: user.last_name,
                               CM_User_Status__c: status,
                               CM_User_Role__c: user.role,
                               CM_User_ID__c: user.id)
    end

    def self.update_user_setup(user)
      salesforce_id = user.salesforce_contact.salesforce_id
      client = CLIENT
      client.update('Contact', Id: salesforce_id, CM_User_Email__c: user.email)
    end

    def self.update_cmupdate(salesforce_id, status)
      client = CLIENT
      client.update('ComfortMaxx_Update__C', Id: salesforce_id, Status__c: status)
    end

    def self.update_all_salesforce_contacts
      users = User.where(current_employee: true).where.not(salesforce_contact_id: nil)

      users.each do |user|
        Salesforce::Contact.update_user(user, 'Active')
        Salesforce::Contact.update_user_setup(user)
      end
    end

    private

    def inactive_user(salesforce_contact)
      @inactive_user ||= UserBuilder.new(@invitation, sf_id).for_invite(salesforce_contact)
    end

  end
end
