class Api::V1::SalesforceController < ApplicationController
  skip_before_filter :require_user
  skip_before_filter :verify_authenticity_token
    # :if => Proc.new { |c| c.request.format == 'application/json' }

  respond_to :json

  def create
    update_object = params[:salesforce].symbolize_keys
    update_type = update_object[:Object__c].parameterize.split('__')[0]

    puts update_object.inspect
    if update_object[:Status__c] == "Logged"
      klass = "Salesforce::#{update_type.classify}".safe_constantize
      if klass.present?
        klass.new(update_object).call
        render :status => 200,
               :json => { :success => true,
                          :info => "success",
                          :data => { }
               }
      else
        render :status => :unprocessable_entity,
               :json => { :success => false,
                          :info => "update failed",
                          :data => { }
               }
      end
    else
      render :status => 200,
             :json => { :success => true,
                        :info => "success",
                        :data => { }
             }
    end

  end
end
